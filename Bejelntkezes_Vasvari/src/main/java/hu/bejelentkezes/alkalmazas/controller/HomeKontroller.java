package hu.bejelentkezes.alkalmazas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeKontroller {
	
	/*
	 * Az alap localhost:8080 oldal - tehát a gyökér mappa
	 */
	@GetMapping("/")
    public String root() {
        return "index";
    }

	/*
	 * Admint fogadó üdvözlő lap
	 */
    @GetMapping("/admin")
    public String userIndex() {
        return "admin/index";
    }

    /*
     * Bejelentkező oldal
     */
    @GetMapping("/login")
    public String login() {
        return "login";
    }
    
    /*
     * Hozzáférés megtadása ha nem jelentkezett be vagy nem megfelelő szerepkörrel próbálkozunk. 
     */
    @GetMapping("/access-denied")
    public String accessDenied() {
        return "/error/access-denied";
    }

}
