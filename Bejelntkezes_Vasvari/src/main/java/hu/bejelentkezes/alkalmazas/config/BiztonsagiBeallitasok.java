package hu.bejelentkezes.alkalmazas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import hu.bejelentkezes.alkalmazas.logging.MegtagadottHozzaferesNaplozasiKezelo;

@EnableWebSecurity
@Configuration
public class BiztonsagiBeallitasok extends WebSecurityConfigurerAdapter {
	
	 @Autowired
	 private MegtagadottHozzaferesNaplozasiKezelo mhnk;
	
	 /*
	  * Itt tudjuk megszabni milyen oldalakhoz férjunk hozzá. 
	  * pl.: "/" engedélyeztem a többi oldalt viszont nem.
	  * 	a "/user/**" mappában lévő weboldalakat csak a USER szerepkörrel rendelkező felhasználó férhet hozzá.
	  * 
	  * Ha nem készítünk saját bejelntkező oldalt akkor a Spring automatikusan add nekünk egy készet.
	  * 
	  * ExceptionHandlerünket ugye a MegtagadottHozzaferesNaplozasiKezelo-ben hoztuk létre. Amennyiben nem megfelelő
	  * szerepkörrel szeretnénk megnyitni oldalt akkor átfog irányítani minket a hozzáférés megtagad oldalra!
	  */
	 @Override
	 protected void configure(HttpSecurity http) throws Exception {
	        http
	                .authorizeRequests()
	                    .antMatchers("/").permitAll()
	                    .antMatchers("/admin/**").hasRole("ADMIN")
	                    .anyRequest().authenticated()
	                .and()
		                .formLogin()
		                    .loginPage("/login")
		                    .permitAll()
                    .and()
	                    .logout()
	                        .invalidateHttpSession(true)
	                        .clearAuthentication(true)
	                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	                        .logoutSuccessUrl("/login?logout")
	                        .permitAll()
	                .and()
						.exceptionHandling()
							.accessDeniedHandler(mhnk);
	    }

	/*
	 * Memoriában tárolt authentikáció - gondolom egyáltalán nem biztonságos nem hiába használunk adatbázis alapút - 
	 * itt tudjuk létrehozni a felhasználót és a hozzá tartozó jelszó párost és megmondani a szerepkörét.
	 */
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
        	.inMemoryAuthentication()
                .withUser("felhasznalo").password(passwordEncoder().encode("jelszo")).roles("USER")
            .and()
                .withUser("admin").password(passwordEncoder().encode("jelszo")).roles("ADMIN");
    }
	
	/*
	 * Spring új verziója megköveteli, hogy a jelszó kódolva legyen így hosszas keresés után ezt a megoldást találtam rá.
	 * Amiért használni kezdtem az az, hogy állandó 'null' értékkel exceptiont dobott bejelentkezéskor a password mezőre.
	 */
	 @Bean
	 public BCryptPasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	 }

}
