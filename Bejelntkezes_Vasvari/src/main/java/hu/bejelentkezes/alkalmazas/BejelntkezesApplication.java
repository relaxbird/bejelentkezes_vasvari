package hu.bejelentkezes.alkalmazas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BejelntkezesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BejelntkezesApplication.class, args);
	}

}
